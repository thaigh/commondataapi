﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonDataApi.Core
{
    public class PolicyExceptionContainer
    {
        private Dictionary<Guid, List<Exception>> _exceptionMap = new Dictionary<Guid, List<Exception>>();

        private readonly object _containerLock = new object();

        public Guid AddContainer()
        {
            lock (_containerLock)
            {
                Guid key = Guid.NewGuid();
                _exceptionMap.Add(key, new List<Exception>());
                return key;
            }
        }

        public void RemoveContainer(Guid key)
        {
            lock (_containerLock)
            {
                if (_exceptionMap.ContainsKey(key))
                {
                    _exceptionMap[key].Clear();
                    _exceptionMap.Remove(key);
                }
            }
        }

        public IEnumerable<Exception> GetExceptions(Guid key)
        {
            List<Exception> exc = new List<Exception>();
            lock (_containerLock)
            {
                if (_exceptionMap.ContainsKey(key))
                    exc.AddRange(_exceptionMap[key]);
            }
            return exc;
        }

        public void AddException(Guid key, Exception e)
        {
            lock(_containerLock)
            {
                if (_exceptionMap.ContainsKey(key))
                    _exceptionMap[key].Add(e);
            }
        }


    }
}
