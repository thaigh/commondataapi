﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonDataApi.Core
{
    /// <summary>
    /// Basic Execution Policy that just calls functions with no additional logic (e.g. retry)
    /// </summary>
    public class DefaultExecutionPolicy : IExecutionPolicy
    {
        public void Execute(Action action)
        {
            action();
        }

        public T ExecuteAndReturn<T>(Func<T> func)
        {
            return func();
        }
    }
}
