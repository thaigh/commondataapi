﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CommonDataApi.Core
{
    public interface ICommonDatabaseApi
    {
        IEnumerable<T> GetAll<T>() where T : class;
        void UploadToTable<T>(IEnumerable<T> records, string tableName);
        IAsyncResult ExecuteCommand(string query, object parameters, int commandTimeout);
        IEnumerable<T> QueryDatabase<T>(string query, object parameters, int commandTimeout);
        void InsertData<T>(T data) where T : class;
        IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, int commandTimeout);
    }

    public interface IExecutionPolicy
    {
        T ExecuteAndReturn<T>(Func<T> func);
        void Execute(Action action);
    }

}