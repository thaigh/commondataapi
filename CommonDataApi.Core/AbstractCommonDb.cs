﻿using System;
using System.Collections.Generic;
using System.IO;

namespace CommonDataApi.Core
{
    public abstract class AbstractCommonDb : ICommonDatabaseApi, IDisposable
    {
        public const int DefaultCommandTimeoutSeconds = 30;

        protected readonly string ConnectionString;
        protected readonly IExecutionPolicy ExecutionPolicy;

        protected AbstractCommonDb(string connectionString)
        {
            ConnectionString = connectionString;
            ExecutionPolicy = new DefaultExecutionPolicy();
        }

        protected AbstractCommonDb(string connectionString, IExecutionPolicy policy)
        {
            ConnectionString = connectionString;
            ExecutionPolicy = policy ?? throw new ArgumentException("Execution policy cannot be null. Use DefaultExecutionPolicy if required", nameof(policy));
        }


        // Abstract implementation of interface
        public abstract IEnumerable<T> GetAll<T>() where T : class;
        public abstract void UploadToTable<T>(IEnumerable<T> records, string tableName);
        public abstract void InsertData<T>(T data) where T : class;
        public abstract IAsyncResult ExecuteCommand(string query, object parameters, int commandTimeout);
        public abstract IEnumerable<T> QueryDatabase<T>(string query, object parameters, int commandTimeout);
        public abstract IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, int commandTimeout);
        public abstract void Dispose();

        // Overloads for ExecuteCommand
        public virtual IAsyncResult ExecuteCommand(string query) => ExecuteCommand(query, new { }, DefaultCommandTimeoutSeconds);
        public virtual IAsyncResult ExecuteCommand(string query, int commandTimeout) => ExecuteCommand(query, new { }, commandTimeout);
        public virtual IAsyncResult ExecuteCommand(string query, TimeSpan commandTimeout) => ExecuteCommand(query, new { }, (int)commandTimeout.TotalSeconds);
        public virtual IAsyncResult ExecuteCommand(string query, object parameters) => ExecuteCommand(query, parameters, DefaultCommandTimeoutSeconds);
        public virtual IAsyncResult ExecuteCommand(string query, object parameters, TimeSpan commandTimeout) => ExecuteCommand(query, parameters, (int)commandTimeout.TotalSeconds);

        // Overloads for QueryDatabase
        public virtual IEnumerable<T> QueryDatabase<T>(string query) => QueryDatabase<T>(query, new int { }, DefaultCommandTimeoutSeconds);
        public virtual IEnumerable<T> QueryDatabase<T>(string query, int commandTimeout) => QueryDatabase<T>(query, new int { }, commandTimeout);
        public virtual IEnumerable<T> QueryDatabase<T>(string query, TimeSpan commandTimeout) => QueryDatabase<T>(query, new int { }, (int)commandTimeout.TotalSeconds);
        public virtual IEnumerable<T> QueryDatabase<T>(string query, object parameters) => QueryDatabase<T>(query, parameters, DefaultCommandTimeoutSeconds);
        public virtual IEnumerable<T> QueryDatabase<T>(string query, object parameters, TimeSpan commandTimeout) => QueryDatabase<T>(query, parameters, (int)commandTimeout.TotalSeconds);

        // Overloads for QueryDatabaseFromFile
        public virtual IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile) => QueryDatabaseFromFile<T>(queryFile, new { }, DefaultCommandTimeoutSeconds);
        public virtual IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, int commandTimeout) => QueryDatabaseFromFile<T>(queryFile, new { }, commandTimeout);
        public virtual IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, TimeSpan commandTimeout) => QueryDatabaseFromFile<T>(queryFile, new { }, (int)commandTimeout.TotalSeconds);
        public virtual IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters) => QueryDatabaseFromFile<T>(queryFile, parameters, DefaultCommandTimeoutSeconds);
        public virtual IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, TimeSpan commandTimeout) => QueryDatabaseFromFile<T>(queryFile, parameters, (int)commandTimeout.TotalSeconds);

    }

}