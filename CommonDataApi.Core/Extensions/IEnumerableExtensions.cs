﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Linq;

namespace CommonDataApi.Core.Extensions
{
    /// <summary>
    /// Mark this C# Class Property as ignorable when
    /// converting the model to a DataTable record
    /// </summary>
    public class IgnoreInDataTableAttribute : Attribute { }

    public static class IEnumerableExtensions
    {
        /// <summary>
        /// Builds a DataTable whereby each row represents an element in the enumerable
        /// collection and each column represents the columns of the underlying type
        /// where the column ordering is derived based on the ordering of properties
        /// in the class
        /// </summary>
        /// <typeparam name="T">The base type to convert to the DataTable</typeparam>
        /// <param name="data">The collection of elements to convert</param>
        /// <returns>A DataTable representing the data collection</returns>
        public static DataTable AsDataTable<T>(this IEnumerable<T> data)
        {
            // Base code adapted from http://blog.developers.ba/bulk-insert-generic-list-sql-server-minimum-lines-code/

            PropertyDescriptorCollection schema = TypeDescriptor.GetProperties(typeof(T));
            var table = new DataTable();

            // When working with data models that are built based on a preexisting
            // Database table, we may add extra calculated properties for C# usability
            // that won't be available in the original data table.
            //
            // To avoid errors in generating an incorrect data table model and then trying to
            // insert into a column that does not exist, the IgnoreInDataTable attribute
            // can be used to discard these columns

            // Create the column headings
            table.DefineSchema(schema);

            // Create each row in the DataTable
            foreach (T item in data)
            {
                table.AddDataRow(item, schema);
            }

            return table;
        }

        private static void DefineSchema(this DataTable table, PropertyDescriptorCollection schema) {
            foreach (PropertyDescriptor prop in schema)
            {
                bool isCalculatedProperty = prop.Attributes.OfType<IgnoreInDataTableAttribute>().Any();
                if (!isCalculatedProperty)
                    table.Columns.Add(prop.Name, Nullable.GetUnderlyingType(prop.PropertyType) ?? prop.PropertyType);
            }
        }

        private static void AddDataRow<T>(this DataTable table, T item, PropertyDescriptorCollection schema) {
            DataRow row = table.NewRow();

            foreach (PropertyDescriptor prop in schema)
            {
                bool isCalculatedProperty = prop.Attributes.OfType<IgnoreInDataTableAttribute>().Any();
                if (!isCalculatedProperty)
                    row[prop.Name] = prop.GetValue(item) ?? DBNull.Value;
            }

            table.Rows.Add(row);
        }

        /// <summary>
        /// Perform a given action on each element in the enumeration
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="enumeration">The source enumeration</param>
        /// <param name="action">The action to perform</param>
        public static void ForEach<T>(this IEnumerable<T> enumeration, Action<T> action)
        {
            // Code taken from http://stackoverflow.com/a/200584

            // Perform the given action on each element in the enumeration
            foreach (T item in enumeration)
            {
                action(item);
            }
        }

    }
}
