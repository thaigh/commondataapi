﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using Dapper;
using Dapper.Contrib.Extensions;

namespace CommonDataApi.Core
{
    public class GenericDatabaseApi : AbstractCommonDb
    {
        private readonly IDbConnection _connection;

        public GenericDatabaseApi(IDbConnection connection, IExecutionPolicy policy) : base(connection.ConnectionString, policy)
        {
            _connection = connection;
        }

        public override IAsyncResult ExecuteCommand(string query, object parameters, int commandTimeout)
        {
            return ExecutionPolicy.ExecuteAndReturn(() =>
            {
                return ExecuteCommandUnwrapped(query, parameters, commandTimeout);
            });
        }

        private IAsyncResult ExecuteCommandUnwrapped(string query, object parameters, int commandTimeout)
        {
            using (_connection)
            {
                IAsyncResult res = _connection.ExecuteAsync(query, parameters, commandTimeout: commandTimeout);
                return res;
            }
        }

        public override IEnumerable<T> GetAll<T>()
        {
            return ExecutionPolicy.ExecuteAndReturn(() =>
            {
                return GetAllUnwrapped<T>();
            });
        }

        private IEnumerable<T> GetAllUnwrapped<T>() where T : class
        {
            using (_connection)
            {
                return _connection.GetAll<T>();
            }
        }

        public override void InsertData<T>(T data)
        {
            ExecutionPolicy.Execute(() =>
            {
                InsertDataUnwrapped(data);
            });
        }

        private void InsertDataUnwrapped<T>(T data) where T : class
        {
            using (_connection)
            {
                _connection.Insert<T>(data);
            }
        }

        public override IEnumerable<T> QueryDatabase<T>(string query, object parameters, int commandTimeout)
        {
            return ExecutionPolicy.ExecuteAndReturn(() =>
            {
                return QueryDatabaseUnwrapped<T>(query, parameters, commandTimeout);
            });
        }

        private IEnumerable<T> QueryDatabaseUnwrapped<T>(string query, object parameters, int commandTimeout) {
            using (_connection)
            {
                return _connection.Query<T>(query, parameters, commandTimeout: commandTimeout);
            }
        }

        public override IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, int commandTimeout)
        {
            string query = ReadQueryFromFile(queryFile);
            return QueryDatabase<T>(query, parameters, commandTimeout);
        }

        public override void UploadToTable<T>(IEnumerable<T> records, string tableName)
        {
            // This is implementation specific
            // and cannot be implemented as a generic method
            throw new NotImplementedException();
        }

        private string ReadQueryFromFile(FileInfo queryFile)
        {
            if (!queryFile.Exists) throw new FileNotFoundException("Query File does not exist");

            return File.ReadAllText(queryFile.FullName);
        }

        public override void Dispose()
        {
            if (_connection != null)
                _connection.Dispose();
        }
    }
}
