﻿namespace CommonDataApi.Core
{
    public enum DatabaseType
    {
        Sql,
        Oracle,
        MySql
    }

    public class DatabaseConnectionConfig
    {
        public readonly string ConnectionString;
        public readonly DatabaseType SqlType;

        public DatabaseConnectionConfig(string connectionString, DatabaseType sqlType)
        {
            ConnectionString = connectionString;
            SqlType = sqlType;
        }
    }

    public class OneWayDataPipeConfiguration
    {
        public DatabaseConnectionConfig FromDatabaseConfig { get; set; }
        public DatabaseConnectionConfig ToDatabaseConfig { get; set; }
    }

}