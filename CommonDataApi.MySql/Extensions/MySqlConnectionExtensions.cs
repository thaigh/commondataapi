﻿using System.Collections.Generic;
using System.Data;
using System.IO;
using CommonDataApi.Core.Extensions;
using MySql.Data.MySqlClient;

namespace CommonDataApi.MySql.Extensions
{
    public static class MySqlConnectionExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        /// <param name="tableName"></param>
        public static void BulkCopyEnumerable<T>(this MySqlConnection connection, IEnumerable<T> data, string tableName)
        {
            connection.BulkCopyDataTable(data.AsDataTable(), tableName);
        }


        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        /// <param name="tableName"></param>
        public static void BulkCopyDataTable(this MySqlConnection connection, DataTable data, string tableName)
        {
            // https://stackoverflow.com/a/30628018
            // https://stackoverflow.com/a/6308289
            // https://www.codeproject.com/Tips/665519/Writing-a-DataTable-to-a-CSV-file

            // Dump table data to a temp CSV file
            string tempCsvFileSpec = Path.GetTempFileName();
            using (StreamWriter writer = new StreamWriter(tempCsvFileSpec))
            {
                Rfc4180Writer w = new Rfc4180Writer(writer);
                w.WriteDataTable(data, false);
            }

            // Bulk Load the data to the database
            var msbl = new MySqlBulkLoader(connection)
            {
                TableName = tableName,
                FileName = tempCsvFileSpec,
                FieldTerminator = Rfc4180Writer.FieldTerminator,
                FieldQuotationCharacter = Rfc4180Writer.FieldQuotationCharacter
            };
            msbl.Load();

            // Delete the temp CSV file
            File.Delete(tempCsvFileSpec);
        }

    }
}