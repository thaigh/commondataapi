﻿using System;
using System.Collections.Generic;
using System.Data;
using System.IO;
using System.Linq;

namespace CommonDataApi.MySql.Extensions
{
    internal class Rfc4180Writer
    {
        // https://www.codeproject.com/Tips/665519/Writing-a-DataTable-to-a-CSV-file

        public const string FieldTerminator          = ",";
        public const char   FieldQuotationCharacter  = '"';
        public const string FieldQuotationEscapeFrom = "\"";
        public const string FieldQuotationEscapeTo   = "\"\"";

        private readonly TextWriter _writer;

        public Rfc4180Writer(TextWriter writer) {
            _writer = writer;
        }

        public void WriteDataTable(DataTable sourceTable, bool includeHeaders)
        {
            if (includeHeaders)
                WriteHeadersToFile(sourceTable.Columns);

            WriteTableRowsToFile(sourceTable.Rows);

            _writer.Flush();
        }

        private void WriteHeadersToFile(DataColumnCollection sourceTableColumns)
        {
            IEnumerable<String> headerValues = sourceTableColumns
                    .OfType<DataColumn>()
                    .Select(c => QuoteValue(c.ColumnName));

            _writer.WriteLine(String.Join(FieldTerminator, headerValues));
        }

        private void WriteTableRowsToFile(DataRowCollection sourceTableRows)
        {
            IEnumerable<String> items = null;

            foreach (DataRow row in sourceTableRows)
            {
                items = row.ItemArray.Select(o => QuoteValue(o?.ToString() ?? String.Empty));
                _writer.WriteLine(String.Join(FieldTerminator, items));
            }
        }

        private static string QuoteValue(string value)
        {
            return String.Concat(
                FieldQuotationCharacter,
                value.Replace(FieldQuotationEscapeFrom, FieldQuotationEscapeTo),
                FieldQuotationCharacter
            );
        }
    }
}