﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonDataApi.Core;
using CommonDataApi.MySql.Extensions;
using MySql.Data.MySqlClient;

namespace CommonDataApi.Sql
{
    public class MySqlDbApi : AbstractCommonDb
    {
        public MySqlDbApi(string connectionString) : base(connectionString) { }
        public MySqlDbApi(string connectionString, IExecutionPolicy policy) : base(connectionString, policy) { }

        private GenericDatabaseApi GenericApi =>
            new GenericDatabaseApi(
                new MySqlConnection(ConnectionString),
                ExecutionPolicy
            );

        public override IEnumerable<T> GetAll<T>()
        {
            return GenericApi.GetAll<T>();
        }

        public override void UploadToTable<T>(IEnumerable<T> records, string tableName)
        {
            // This method uses MySQL Connection specific logic
            // specifically the Bulk Copy implementation and hence
            // cannot be generified.
            ExecutionPolicy.Execute(() =>
            {
                UploadToTableUnwrapped(records, tableName);
            });
        }

        private void UploadToTableUnwrapped<T>(IEnumerable<T> records, string tableName) {
            using (var connection = new MySqlConnection(ConnectionString))
            {
                connection.BulkCopyEnumerable<T>(records, tableName);
            }
        }

        public override IAsyncResult ExecuteCommand(string query, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.ExecuteCommand(query, parameters, commandTimeout);
        }

        public override IEnumerable<T> QueryDatabase<T>(string query, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.QueryDatabase<T>(query, parameters, commandTimeout);
        }

        public override void InsertData<T>(T data)
        {
            GenericApi.InsertData<T>(data);
        }

        public override IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.QueryDatabaseFromFile<T>(queryFile, parameters, commandTimeout);
        }

        public override void Dispose() { }

    }
}