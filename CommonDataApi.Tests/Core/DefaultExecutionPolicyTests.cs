using CommonDataApi.Core;
using CommonDataApi.Tests.Mock;
using System;
using Xunit;

namespace CommonDataApi.Tests.Core
{
    public class DefaultExecutionPolicyTests
    {
        [Fact]
        public void ExecuteAction_IncrementsCounter()
        {
            MockCounter counter = new MockCounter();
            DefaultExecutionPolicy policy = new DefaultExecutionPolicy();

            policy.Execute(counter.Increment);

            Assert.Equal(1, counter.Value);
        }

        [Fact]
        public void ExecuteAndReturn_IncrementsCounter()
        {
            MockCounter counter = new MockCounter();
            DefaultExecutionPolicy policy = new DefaultExecutionPolicy();

            int actual = policy.ExecuteAndReturn(counter.GetCounterValue);

            Assert.Equal(0, actual);
        }
    }
}
