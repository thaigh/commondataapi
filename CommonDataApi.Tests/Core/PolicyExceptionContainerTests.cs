﻿using CommonDataApi.Core;
using System;
using System.Collections.Generic;
using System.Linq;
using Xunit;

namespace CommonDataApi.Tests.Core
{
    public class PolicyExceptionContainerTests
    {
        [Fact]
        public void AddContainer_AddsEmptyContainer()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();
            IEnumerable<Exception> exceptions = container.GetExceptions(key);
            Assert.Empty(exceptions);
        }

        [Fact]
        public void AddException_AddsOneException()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();

            Exception e = new Exception("Something went wrong");
            container.AddException(key, e);

            IEnumerable<Exception> exceptions = container.GetExceptions(key);
            Assert.Single(exceptions);
            Assert.Equal(e.Message, exceptions.First().Message);
        }

        [Fact]
        public void AddException_DoesNothingWhenKeyNotRegisterd()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();
            Guid newKey = Guid.NewGuid();
            Assert.NotEqual(key, newKey);

            Exception e = new Exception("Something went wrong");
            container.AddException(newKey, e);

            IEnumerable<Exception> exceptions = container.GetExceptions(newKey);
            Assert.Empty(exceptions);
        }

        [Fact]
        public void GetExceptions_GetsAddedExceptions()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();

            Exception e = new Exception("Something went wrong");
            container.AddException(key, e);

            IEnumerable<Exception> exceptions = container.GetExceptions(key);
            Assert.Single(exceptions);
            Assert.Equal(e.Message, exceptions.First().Message);
        }

        [Fact]
        public void GetExceptions_ReturnsEmptyWhenInvalidKeyUsed()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();
            Guid newKey = Guid.NewGuid();
            Assert.NotEqual(key, newKey);

            Exception e = new Exception("Something went wrong");
            container.AddException(key, e);

            IEnumerable<Exception> exceptions = container.GetExceptions(newKey);
            Assert.Empty(exceptions);
        }

        [Fact]
        public void RemoveContainer_RemovesExceptionsAndContainer()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();

            Exception e = new Exception("Something went wrong");
            container.AddException(key, e);

            IEnumerable<Exception> exceptions = container.GetExceptions(key);
            Assert.Single(exceptions);
            Assert.Equal(e.Message, exceptions.First().Message);

            container.RemoveContainer(key);

            exceptions = container.GetExceptions(key);
            Assert.Empty(exceptions);
        }

        [Fact]
        public void RemoveContainer_DoesNothingWhenNonRegisteredKeyUsed()
        {
            PolicyExceptionContainer container = new PolicyExceptionContainer();
            Guid key = container.AddContainer();
            Guid newKey = Guid.NewGuid();
            Assert.NotEqual(key, newKey);

            Exception e = new Exception("Something went wrong");
            container.AddException(key, e);

            IEnumerable<Exception> exceptions = container.GetExceptions(key);
            Assert.Single(exceptions);
            Assert.Equal(e.Message, exceptions.First().Message);

            container.RemoveContainer(newKey);

            exceptions = container.GetExceptions(key);
            Assert.Single(exceptions);
            Assert.Equal(e.Message, exceptions.First().Message);
        }

    }
}
