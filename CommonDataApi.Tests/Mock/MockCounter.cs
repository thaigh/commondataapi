﻿using System;
using System.Collections.Generic;
using System.Text;

namespace CommonDataApi.Tests.Mock
{
    class MockCounter
    {
        public int Value { get; private set; } = 0;
        public void Increment() { Value++; }
        public int GetCounterValue() { return Value; }
    }
}
