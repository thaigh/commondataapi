﻿using System;
using System.Collections.Generic;
using System.Data;
using System.Data.SqlClient;
using CommonDataApi.Core.Extensions;

namespace CommonDataApi.Sql.Extensions
{
    public static class SqlConnectionExtensions
    {
        /// <summary>
        /// 
        /// </summary>
        /// <typeparam name="T"></typeparam>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        /// <param name="tableName"></param>
        /// <param name="batchSize"></param>
        public static void BulkCopyEnumerable<T>(this SqlConnection connection, IEnumerable<T> data, string tableName, int batchSize = 100)
        {
            connection.BulkCopyDataTable(data.AsDataTable(), tableName, batchSize);
        }

        /// <summary>
        /// 
        /// </summary>
        /// <param name="connection"></param>
        /// <param name="data"></param>
        /// <param name="tableName"></param>
        /// <param name="batchSize"></param>
        public static void BulkCopyDataTable(this SqlConnection connection, DataTable data, string tableName, int batchSize = 100)
        {
            // Base code taken from http://blog.developers.ba/bulk-insert-generic-list-sql-server-minimum-lines-code/

            if (connection.State != ConnectionState.Open)
                connection.Open();

            SqlTransaction transaction = connection.BeginTransaction();

            using (SqlBulkCopy bulkCopy = new SqlBulkCopy(connection, SqlBulkCopyOptions.Default, transaction))
            {
                bulkCopy.BatchSize = batchSize;
                bulkCopy.DestinationTableName = tableName;
                try
                {
                    bulkCopy.WriteToServer(data);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    connection.Close();

                    throw e;
                }
            }

            transaction.Commit();
        }
    }
}