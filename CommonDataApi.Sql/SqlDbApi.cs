﻿using System;
using System.Collections.Generic;
using System.Data.SqlClient;
using System.IO;
using CommonDataApi.Core;
using CommonDataApi.Sql.Extensions;

namespace CommonDataApi.Sql
{
    public class SqlDbApi : AbstractCommonDb
    {
        public SqlDbApi(string connectionString) : base(connectionString) { }
        public SqlDbApi(string connectionString, IExecutionPolicy policy) : base(connectionString, policy) { }

        // I toyed with the idea of having a single GenericDatabaseApi
        // for the SqlDbApi instance and then just calling the methods.
        // However the issue around disposing the SQL Connection whereby
        // the connection information is discarded (rendering the SQL connection
        // instance unusable) meant that I would need to store extra
        // information locally in the Generic API and hack the connection
        // to force a reconnection. I didn't like this idea, so I'm opting to
        // create a new Generic DB instance every time we need to call
        // some functionality

        private GenericDatabaseApi GenericApi =>
            new GenericDatabaseApi(
                new SqlConnection(ConnectionString),
                ExecutionPolicy
            );

        public override IEnumerable<T> GetAll<T>()
        {
            return GenericApi.GetAll<T>();
        }

        public override void UploadToTable<T>(IEnumerable<T> records, string tableName)
        {
            // This method uses SQL Connection specific logic
            // specifically the Bulk Copy implementation and hence
            // cannot be generified.
            ExecutionPolicy.Execute(() =>
            {
                UploadToTableUnwrapped(records, tableName);
            });
        }

        private void UploadToTableUnwrapped<T>(IEnumerable<T> records, string tableName) {
            using (var connection = new SqlConnection(ConnectionString))
            {
                connection.BulkCopyEnumerable<T>(records, tableName);
            }
        }

        public override IAsyncResult ExecuteCommand(string query, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.ExecuteCommand(query, parameters, commandTimeout);
        }

        public override IEnumerable<T> QueryDatabase<T>(string query, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.QueryDatabase<T>(query, parameters, commandTimeout);
        }

        public override void InsertData<T>(T data)
        {
            GenericApi.InsertData<T>(data);
        }

        public override IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.QueryDatabaseFromFile<T>(queryFile, parameters, commandTimeout);
        }

        public override void Dispose() { }

    }
}