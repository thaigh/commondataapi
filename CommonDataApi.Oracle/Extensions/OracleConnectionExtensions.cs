﻿using System;
using System.Collections.Generic;
using System.Data;
using CommonDataApi.Core.Extensions;
using Oracle.ManagedDataAccess.Client;
using Oracle.ManagedDataAccess.Client.BulkCopy;

namespace CommonDataApi.Oracle.Extensions
{
    public static class OracleConnectionExtensions
    {
        public static void BulkCopyEnumerable<T>(this OracleConnection connection, IEnumerable<T> data, string tableName, int batchSize = 100)
        {
            connection.BulkCopyDataTable(data.AsDataTable(), tableName, batchSize);
        }

        public static void BulkCopyDataTable(this OracleConnection connection, DataTable data, string tableName, int batchSize = 100)
        {
            if (connection.State != ConnectionState.Open)
                connection.Open();

            OracleTransaction transaction = connection.BeginTransaction();

            using (OracleBulkCopy bulkCopy = new OracleBulkCopy(connection, transaction))
            {
                bulkCopy.BatchSize = batchSize;
                bulkCopy.DestinationTableName = tableName;
                try
                {
                    bulkCopy.WriteToServer(data);
                }
                catch (Exception e)
                {
                    transaction.Rollback();
                    connection.Close();

                    throw e;
                }
            }

            transaction.Commit();
        }
    }
}