﻿using System;
using System.Collections.Generic;
using System.IO;
using CommonDataApi.Core;
using CommonDataApi.Oracle.Extensions;
using Oracle.ManagedDataAccess.Client;

namespace CommonDataApi.Oracle
{
    public class OracleDbApi : AbstractCommonDb
    {
        public OracleDbApi(string connectionString) : base(connectionString) { }
        public OracleDbApi(string connectionString, IExecutionPolicy policy) : base(connectionString, policy) { }

        private GenericDatabaseApi GenericApi =>
            new GenericDatabaseApi(
                new OracleConnection(ConnectionString),
                ExecutionPolicy
            );

        public override IEnumerable<T> GetAll<T>()
        {
            return GenericApi.GetAll<T>();
        }

        public override void UploadToTable<T>(IEnumerable<T> records, string tableName)
        {
            // This method uses Oracle SQL Connection specific logic
            // specifically the Bulk Copy implementation and hence
            // cannot be generified.
            ExecutionPolicy.Execute(() =>
            {
                UploadToTableUnwrapped(records, tableName);
            });
        }

        private void UploadToTableUnwrapped<T>(IEnumerable<T> records, string tableName) {
            using (var connection = new OracleConnection(ConnectionString))
            {
                connection.BulkCopyEnumerable<T>(records, tableName);
            }
        }

        public override IAsyncResult ExecuteCommand(string query, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.ExecuteCommand(query, parameters, commandTimeout);
        }

        public override IEnumerable<T> QueryDatabase<T>(string query, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.QueryDatabase<T>(query, parameters, commandTimeout);
        }

        public override void InsertData<T>(T data)
        {
            GenericApi.InsertData<T>(data);
        }

        public override IEnumerable<T> QueryDatabaseFromFile<T>(FileInfo queryFile, object parameters, int commandTimeout = DefaultCommandTimeoutSeconds)
        {
            return GenericApi.QueryDatabaseFromFile<T>(queryFile, parameters, commandTimeout);
        }

        public override void Dispose() { }
    }
}