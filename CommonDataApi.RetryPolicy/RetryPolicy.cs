﻿using System;
using Polly;
using System.Collections.Generic;
using CommonDataApi.Core;

namespace CommonDataApi.RetryPolicy
{

    public class RetryPolicy : IExecutionPolicy
    {
        public const int DefaultRetryAttempts = 3;
        public const int DefaultRetryTimeoutSeconds = 5;

        private const string ExecutionPolicyContainerKeyName = "ExecutionPolicyContainerGuid";
        private const string PolicyMethodKeyName = "Operation";

        public event EventHandler<RetryPolicyExceptionContext> OnPolicyError;

        private readonly int _retryAttempts;
        private readonly TimeSpan _retryTimeout;
        private readonly Policy _policy;

        private PolicyExceptionContainer _policyExceptions = new PolicyExceptionContainer();


        public RetryPolicy() : this(DefaultRetryAttempts, TimeSpan.FromSeconds(DefaultRetryTimeoutSeconds)) { }
        public RetryPolicy(int retryAttempts, TimeSpan retryTimeout)
        {
            PreconditionCheckRetryAttempts(retryAttempts);
            PreconditionCheckRetryTimeout(retryTimeout);

            this._retryAttempts = retryAttempts;
            this._retryTimeout = retryTimeout;

            this._policy = BuildPolicy();
        }

        private void PreconditionCheckRetryAttempts(int retryAttempts)
        {
            if (retryAttempts < 0)
                throw new ArgumentException("Retry attempts must be a positive integer");
        }

        private void PreconditionCheckRetryTimeout(TimeSpan retryTimeout)
        {
            if (retryTimeout.Ticks < 0)
                throw new ArgumentException("Retry timeout must be positive");
        }


        private Policy BuildPolicy()
        {
            return Policy
                .Handle<Exception>()
                .WaitAndRetry(
                    _retryAttempts,
                    currentRetryAttempt => _retryTimeout,
                    (exception, timespan, context) => EmitPolicyError(exception, timespan, context)
                );
        }


        private Dictionary<string, object> BuildPolicyExecutionContextData(Guid policyExceptionKey, string methodName)
        {
            Dictionary<string, object> dict = new Dictionary<string, object>();
            dict.Add(ExecutionPolicyContainerKeyName, policyExceptionKey);
            dict.Add(PolicyMethodKeyName, methodName);
            return dict;
        }


        public T ExecuteAndReturn<T>(Func<T> action)
        {
            Guid policyExceptionKey = _policyExceptions.AddContainer();
            var contextData = BuildPolicyExecutionContextData(policyExceptionKey, action.Method.Name);

            var policyResult = _policy.ExecuteAndCapture(
                action: context => action.Invoke(),
                contextData: contextData
            );

            if (policyResult.Outcome == OutcomeType.Failure)
            {
                IEnumerable<Exception> exceptions = _policyExceptions.GetExceptions(policyExceptionKey);
                _policyExceptions.RemoveContainer(policyExceptionKey);
                throw new PolicyFailureException(_retryAttempts, new AggregateException(exceptions));
            }

            // Clean up policy exeption container
            _policyExceptions.RemoveContainer(policyExceptionKey);

            return policyResult.Result;
        }

        public void Execute(Action action)
        {
            Guid policyExceptionKey = _policyExceptions.AddContainer();
            var contextData = BuildPolicyExecutionContextData(policyExceptionKey, action.Method.Name);

            var policyResult = _policy.ExecuteAndCapture(
                action: context => action.Invoke(),
                contextData: contextData
            );

            if (policyResult.Outcome == OutcomeType.Failure)
            {
                IEnumerable<Exception> exceptions = _policyExceptions.GetExceptions(policyExceptionKey);
                _policyExceptions.RemoveContainer(policyExceptionKey);
                throw new PolicyFailureException(_retryAttempts, new AggregateException(exceptions));
            }

            // Clean up policy exeption container
            _policyExceptions.RemoveContainer(policyExceptionKey);
        }

        private void EmitPolicyError(Exception exception, TimeSpan sleepDuration, Context context) {
            Guid policyExceptionKey = (Guid)context[ExecutionPolicyContainerKeyName];
            _policyExceptions.AddException(policyExceptionKey, exception);

            this.OnPolicyError?.Invoke(
                this,
                new RetryPolicyExceptionContext(exception, sleepDuration, context)
            );
        }
    }
}