﻿using System;

namespace CommonDataApi.RetryPolicy
{
    public class PolicyFailureException : Exception
    {
        public readonly int RetryAttempts;
        public readonly AggregateException Exceptions;

        public PolicyFailureException(int attempts, AggregateException exceptions)
            : this(attempts, exceptions, $"Could not successfully execute Policy after {attempts} attempts") { }

        public PolicyFailureException(int attempts, AggregateException exceptions, string message) : base(message)
        {
            RetryAttempts = attempts;
            Exceptions = exceptions;
        }
    }
}