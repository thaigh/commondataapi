﻿using System;
using Polly;

namespace CommonDataApi.RetryPolicy
{
    public class RetryPolicyExceptionContext
    {
        public readonly Exception ExceptionThrown;
        public readonly TimeSpan CurrentSleepDuration;
        public readonly Context PollyContext;

        public RetryPolicyExceptionContext(Exception exception, TimeSpan sleepDuration, Context context)
        {
            ExceptionThrown = exception;
            CurrentSleepDuration = sleepDuration;
            PollyContext = context;
        }
    }
}